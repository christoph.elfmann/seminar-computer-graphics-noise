# seminar-computer-graphics-perlin-noise

Simple ray tracing engine used to recreate some of the renders 
from Ken Perlin's 1985 *SIGGRAPH* paper "An image synthesizer" \[1\], 
in which *Perlin noise* was first introduced. 
The implementation is based on his original C code from 1983 \[2\], 
which is included (see `noise/original_perlin.c`).

The created images were used for a report in the 
"Computer Graphics" seminar by Prof. Dr. Winfried Kurth 
(winter term 2019/2020, Göttingen University).

Here is an example of a render featuring Perlin noise:

![Perlin noise sphere](output/demo_perlin_red.png)

By Christoph Elfmann.

\[1\] Ken Perlin. An image synthesizer. *SIGGRAPH Comput. Graph.*,
19(3):287-296, July 1985.

\[2\] Ken Perlin. Noise and turbulence. 
<https://mrl.nyu.edu/~perlin/doc/oscar.html>. 
Last accessed: 2020-04-03.