import math

import numpy as np


class Quaternion:
    """Represents quaternion."""

    def __init__(self, w: float = 0.0, x: float = 0.0,
                 y: float = 0.0, z: float = 0.0):
        self.values = np.array([w, x, y, z])

    @property
    def w(self) -> float:
        return self.values[0]

    @w.setter
    def w(self, value):
        self.values[0] = value

    @property
    def x(self) -> float:
        return self.values[1]

    @x.setter
    def x(self, value):
        self.values[1] = value

    @property
    def y(self) -> float:
        return self.values[2]

    @y.setter
    def y(self, value):
        self.values[2] = value

    @property
    def z(self) -> float:
        return self.values[3]

    @z.setter
    def z(self, value):
        self.values[3] = value

    def get_vector_values(self) -> np.ndarray:
        return self.values[1:4]

    def __str__(self):
        return str(self.values)

    @staticmethod
    def from_euler(roll: float, pitch: float, yaw: float) -> 'Quaternion':
        """Create quaternion from Euler angles.

        Formula taken from https://en.wikipedia.org/wiki/
        Conversion_between_quaternions_and_Euler_angles
        #Euler_Angles_to_Quaternion_Conversion
        (last accessed: 2020-05-18).
        """
        roll = math.radians(roll)
        pitch = math.radians(pitch)
        yaw = math.radians(yaw)

        cy = math.cos(yaw * 0.5)
        sy = math.sin(yaw * 0.5)
        cp = math.cos(pitch * 0.5)
        sp = math.sin(pitch * 0.5)
        cr = math.cos(roll * 0.5)
        sr = math.sin(roll * 0.5)

        w = cr * cp * cy + sr * sp * sy
        x = sr * cp * cy - cr * sp * sy
        y = cr * sp * cy + sr * cp * sy
        z = cr * cp * sy - sr * sp * cy

        return Quaternion(w, x, y, z)
