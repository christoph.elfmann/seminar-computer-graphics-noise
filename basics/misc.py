import math
from typing import Optional, Tuple


def solve_quadratic(a: float, b: float, c: float) \
        -> Optional[Tuple[float, float]]:
    """Return solutions of quadratic equation (ax^2 + bx + c = 0).

    Formula taken from https://www.scratchapixel.com/lessons/
    3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/
    ray-sphere-intersection (last accessed: 2020-05-18).
    """
    discr = b ** 2 - 4 * a * c

    if discr < 0:
        return None

    if discr == 0:
        x = -0.5 * b / a
        return x, x

    q = -0.5 * (b + math.sqrt(discr)) if b > 0 \
            else -0.5 * (b - math.sqrt(discr))

    return q / a, c / q
