import numpy as np

from basics.quaternion import Quaternion


class Vector:
    """Represents vector in 3D space."""

    def __init__(self, x: float = 0.0, y: float = 0.0, z: float = 0.0):
        self.coords = np.array([x, y, z])

    @property
    def x(self) -> float:
        return self.coords[0]

    @x.setter
    def x(self, value):
        self.coords[0] = value

    @property
    def y(self) -> float:
        return self.coords[1]

    @y.setter
    def y(self, value):
        self.coords[1] = value

    @property
    def z(self) -> float:
        return self.coords[2]

    @z.setter
    def z(self, value):
        self.coords[2] = value

    def __str__(self):
        return str(self.coords)

    def magnitude(self) -> float:
        return np.linalg.norm(self.coords)

    def normalize(self) -> 'Vector':
        magnitude = self.magnitude()

        return Vector(*(self.coords / magnitude)) \
            if magnitude != 0 else Vector()


    def rotate(self, rotation: Quaternion) -> 'Vector':
        """Rotate by quaternion.

        Formula taken from https://en.wikipedia.org/wiki/
        Euler%E2%80%93Rodrigues_formula#Vector_formulation
        (last accessed: 2020-05-18).
        """
        a = rotation.w
        w = rotation.get_vector_values()
        cross_w_x = np.cross(w, self.coords)
        rotated = self.coords + 2 * a * cross_w_x + 2 * np.cross(w, cross_w_x)
        rotated = Vector(*rotated)
        rotated.coords *= self.magnitude() / rotated.magnitude()

        return rotated

