from typing import List, Tuple, Optional

from primitives.primitive import Primitive
from basics.vector import Vector


class Ray:
    """Represents a ray of light cast from an origin in a direction."""

    def __init__(self, origin: Vector, direction: Vector):
        """Create ray from origin and direction."""
        self.origin = origin
        self.direction = direction.normalize()

    @staticmethod
    def from_points(origin: Vector, target: Vector) -> 'Ray':
        """Create ray with origin in direction of target point."""
        normal = Vector(*(target.coords - origin.coords))
        return Ray(origin, normal.normalize())

    def cast(self, scene_objects: List[Primitive]) \
            -> Optional[Tuple[float, Primitive]]:
        """Cast ray and return hit of object closest to ray origin."""
        closest_rayhit_distance = None
        closest_object = None

        for scene_object in scene_objects:
            rayhit_distance = scene_object.calc_ray_hit_distance(self)

            if not rayhit_distance:
                continue

            if not closest_rayhit_distance \
                    or rayhit_distance < closest_rayhit_distance:
                closest_rayhit_distance = rayhit_distance
                closest_object = scene_object

        return (closest_rayhit_distance, closest_object) \
            if closest_rayhit_distance else None
