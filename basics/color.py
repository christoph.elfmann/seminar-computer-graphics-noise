import numpy as np


class Color:
    """Represents RGB color tuple.

    RGB values not contained [0, 1] get clamped when calculating
    pixel color.
    """

    def __init__(self, r: float = 0.0, g: float = 0.0, b: float = 0.0):
        self.values = np.array([r, g, b], dtype=float)

    @property
    def r(self) -> float:
        return self.values[0]

    @r.setter
    def r(self, value):
        self.values[0] = value

    @property
    def g(self) -> float:
        return self.values[1]

    @g.setter
    def g(self, value):
        self.values[1] = value

    @property
    def b(self) -> float:
        return self.values[2]

    @b.setter
    def b(self, value):
        self.values[2] = value

    def __str__(self):
        return str(self.values)
