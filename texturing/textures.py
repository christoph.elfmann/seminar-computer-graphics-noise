from abc import ABC, abstractmethod
from typing import Callable

from basics.color import Color
from basics.vector import Vector


class Texture(ABC):
    """Assigns color values to surface points."""

    @abstractmethod
    def calc_color(self, surface_point: Vector) -> Color:
        """Calculate color at a given surface point."""
        raise NotImplementedError


class PlainTexture(Texture):
    """Represents monochrome texture."""

    def __init__(self, color: Color):
        """Create plain texture with given color."""
        super().__init__()
        self.color = color

    def calc_color(self, surface_point: Vector) -> Color:
        """Overrides method in 'Texture'."""
        return self.color


class SolidTexture(Texture):
    """Represents a Solid Texture (see README.md for references)."""

    def __init__(self, color, space_function: Callable[[Vector], Color],
                 scale: float = 1):
        """Creates Solid Texture, sets space function and its scaling."""
        super().__init__()
        self.color = color
        self.space_function = space_function
        self.factor = 1 / scale

    def calc_color(self, surface_point: Vector) -> Color:
        """Overrides method in 'Texture'.

        The color of the Solid Texture is multiplied by the output
        of the space function with scaled input.
        """
        scaled_surface_point = Vector(*(self.factor * surface_point.coords))
        return Color(*(self.color.values
                       * self.space_function(scaled_surface_point).values))
