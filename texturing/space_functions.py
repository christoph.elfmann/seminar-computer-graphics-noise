from noise.original_perlin import noise3
from basics.color import Color
from basics.vector import Vector


def noise_intensity(surface_point: Vector) -> Color:
    """Returns Perlin noise as simple color intensity."""
    value = noise3(surface_point.coords) / (0.65 * 2) + 0.5
    return Color(value, value, value)


colorful_colors = [Color(1, 1, 1), Color(1, 0, 0), Color(0, 1, 0), Color(0, 0, 1)]


def noise_colorful(surface_point: Vector) -> Color:
    """Returns one of several colors determined by Perlin noise value."""
    value = noise3(surface_point.coords)

    if value < -0.19:
        index = 0
    elif value < 0:
        index = 1
    elif value < 0.125:
        index = 2
    else:
        index = 3

    return colorful_colors[index]


def noise_globe(surface_point: Vector) -> Color:
    """Returns one of several colors determined by Perlin noise value.

    The colors are chosen to resemble a map.
    """
    value = noise3(surface_point.coords)

    if value < -0.1:
        return Color(0.1, 0.1, 0.9)
    elif value < 0.0:
        return Color(0.1, 0.5, 0.9)
    elif value < 0.05:
        return Color(1, 0.9, 0.6)
    else:
        return Color(0.2, 0.8, 0.0)
