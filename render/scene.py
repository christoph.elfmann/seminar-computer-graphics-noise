from typing import List, Tuple

from PIL import Image

from render.camera import Camera
from lights.light import Light
from primitives.primitive import Primitive
from basics.color import Color
from basics.vector import Vector


class Scene:
    """Arrangement of objects in 3D space rendered by a camera."""

    def __init__(self, main_camera: Camera, background_color: Color = Color(),
                 ambient_light: Color = Color(1.0, 1.0, 1.0),
                 fog_distance: float = 100):
        """Creates a scene and sets general rendering options.

        :param main_camera: default camera is used to render image
        :param background_color: color of background in rendered image
        :param ambient_light: color intensity of surface points that
                              are not lit by light sources
        :param fog_distance: maximum render distance of objects
        """
        self.background_color = background_color
        self.ambient_light = ambient_light
        self.objects: List[Primitive] = []
        self.lights: List[Light] = []
        self.main_camera = main_camera
        self.fog_distance = fog_distance

    def render(self, resolution: Tuple[int, int] = (512, 512),
               camera: Camera = None) -> Image:
        """Render scene using the specified resolution and camera

        If no camera is specified, the main camera is used.
        At the moment, color intensities produced by different light
        sources are just added up and clamped to a [0, 1] range for
        each of the RGB channels.

        :param resolution: resolution of rendered image
        :param camera: camera for render
        :return: PIL image object in RGB mode
        """
        if not camera:
            camera = self.main_camera

        img = Image.new('RGB', resolution,
                        tuple(self.background_color.values.astype(int)))
        pixels = img.load()

        width, height = resolution

        # ray tracing
        for i, (ray, img_x, img_y) in enumerate(camera.generate_rays(resolution)):
            rayhit = ray.cast(self.objects)

            if not rayhit:
                continue

            closest_rayhit_distance, closest_object = rayhit

            if closest_rayhit_distance >= self.fog_distance:
                continue

            # surface is visible
            surface_point = ray.origin.coords \
                            + closest_rayhit_distance \
                            * ray.direction.coords

            surface_point = Vector(*surface_point)

            surface_normal = closest_object.calc_surface_normal_at(surface_point)

            # add up color intensities
            light = Color(0, 0, 0)
            light.values += self.ambient_light.values

            for light_source in self.lights:
                light.values += light_source.calc_color_at(
                    surface_point, surface_normal, ray, self.objects
                ).values

            fog_factor = closest_rayhit_distance / self.fog_distance

            # additive mixing
            rgb = closest_object.calc_color(surface_point)
            color = fog_factor * self.background_color.values \
                    + (1 - fog_factor) * light.values * rgb.values

            color = tuple(color.clip(0.0, 255.0).round().astype(int))

            pixels[img_x, img_y] = color

            if i % 100 == 0:
                print(f"\r{(i * 100 / (width * height)):5.1f} %", end="")

        print(f"\r100.0 %")

        return img
