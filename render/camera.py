import math
from typing import Tuple

from basics.quaternion import Quaternion
from basics.ray import Ray
from basics.vector import Vector


class Camera:
    """Represents camera providing a rectangular viewport for a scene."""

    def __init__(self, position: Vector, aov: float = 40,
                 rotation_euler: Vector = Vector()):
        """
        Create camera with certain position, rotation and angle of view.

        The camera is created at the world origin in positive z
        direction.

        :param position: position in scene
        :param aov: horizontal angle of view in degrees
        :param rotation_euler: rotation around X, Y and Z axes
        """
        self.position = position
        self.aov = aov

        self.local_unit_x = Vector(1, 0, 0)
        self.local_unit_y = Vector(0, 1, 0)
        self.local_unit_z = Vector(0, 0, 1)

        self.euler_angles = rotation_euler
        self.rotation = Quaternion.from_euler(*rotation_euler.coords)

        self.local_unit_x = self.local_unit_x.rotate(self.rotation)
        self.local_unit_y = self.local_unit_y.rotate(self.rotation)
        self.local_unit_z = self.local_unit_z.rotate(self.rotation)

    def get_direction(self) -> Vector:
        """Return direction camera is facing (center of viewport)."""
        return self.local_unit_z

    def generate_rays(self, resolution: Tuple[int, int]) \
            -> Tuple[Ray, int, int]:
        """Return ray emitted from camera for every viewport pixel.

        The rays are produced row-wise from the top-left corner of the
        viewport down to the bottom from left ro right. If the camera
        is in positive Z direction, greater X/Y coordinates in world
        space correspond to smaller X/Y coordinates in pixel space
        respectively.

        :param resolution: resolution of image rendered from viewport
        :return: tuple of rays emitted from camera and corresponding
                 pixel space coordinates
        """
        width, height = resolution

        aov_h = math.radians(self.aov)

        h = math.tan(aov_h / 2)
        v = h * height / width

        top_left_corner = self.position.coords \
                          + self.local_unit_z.coords \
                          + h * self.local_unit_x.coords \
                          + v * self.local_unit_y.coords

        projection_point = Vector()

        for img_y in range(height):
            span_v = img_y / (height - 1)

            row_point = top_left_corner \
                        - span_v * 2 * v * self.local_unit_y.coords

            for img_x in range(width):
                span_h = img_x / (width - 1)

                projection_point.coords = row_point \
                    - span_h * 2 * h * self.local_unit_x.coords

                ray = Ray.from_points(self.position, projection_point)

                yield ray, img_x, img_y






