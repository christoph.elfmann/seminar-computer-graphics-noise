import math
import random
from typing import Sequence

import numpy as np
from matplotlib import pyplot as plt, cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

from noise.original_perlin import noise1, noise2


def plot_noise_1d():
    """Example plot for 1D Perlin noise."""
    left = -5
    right = 5
    x = np.linspace(left, right, 100)
    noise = [noise1(x) for x in x]

    fig, ax = plt.subplots()

    ax.plot(x, noise)
    ax.set_aspect(1)
    ax.set_xticks(range(left, right + 1))
    ax.set_ylim(-1, 1)
    ax.grid()
    plt.savefig("noise1d.png", bbox_inches='tight')
    plt.show()


def plot_noise_2d():
    """Example plot for 2D Perlin noise."""
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Make data.
    X = np.arange(-5, 5, 0.25)
    Y = np.arange(-5, 5, 0.25)
    grid_X, grid_Y = np.meshgrid(X, Y)
    Z = np.zeros((len(X), len(Y)))

    for i, x in enumerate(X):
        for j, y in enumerate(Y):
            Z[i, j] = noise2((x, y))

    # Plot the surface.
    surf = ax.plot_surface(grid_X, grid_Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)

    # Customize the z axis.
    ax.set_zlim(-1.01, 1.01)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.savefig("noise2d.png", bbox_inches='tight')
    plt.show()


def plot_turbulence(lims: Sequence[float] = (0.0, 1.0),
                    num_octaves: int = 4,
                    persistence: float = 0.5,
                    lacunarity: float = 2.0,
                    start_amplitude: float = 1.0,
                    start_frequency: float = 1.0):
    """Example plot for Perlin noise turbulence."""
    x = np.linspace(*lims, 100)

    amplitude = start_amplitude
    frequency = start_frequency

    fig, axes = plt.subplots(1, num_octaves)

    y_sum = np.zeros(100)

    for octave in range(num_octaves):
        y_octave = amplitude * np.array([noise1(x * frequency) for x in x])


        axes[octave].plot(x, y_octave)
        axes[octave].set_aspect(1)
        axes[octave].set_xticks(range(math.floor(lims[0]), math.ceil(lims[1]) + 1))
        axes[octave].set_ylim(-0.5, 0.5)

        axes[octave].axhline(0, ls='--', color='gray')


        if octave > 0:
            axes[octave].set_yticks([])

        axes[octave].set_title(f"Octave {octave}")
        amplitude *= persistence
        frequency *= lacunarity
        y_sum += y_octave

    fig.savefig("octaves.png", bbox_inches='tight')

    fig, ax = plt.subplots()
    ax.plot(x, y_sum)
    ax.set_aspect(1)
    ax.set_xticks(range(math.floor(lims[0]), math.ceil(lims[1]) + 1))
    ax.set_ylim(-0.5, 0.5)
    ax.set_title(f"Octave sum")
    ax.axhline(0, ls='--', color='gray')
    fig.savefig("octaves_sum.png", bbox_inches='tight')
    plt.show()


if __name__ == '__main__':
    """Plot some examples of Perlin noise and turbulence in 1D/2D."""
    random.seed(1)
    plot_noise_1d()
    plot_noise_2d()
    plot_turbulence()
