import math
import random as rand

import numpy as np

# The following is supposed to be a verbatim translation of Ken Perlin's
# 1983 original implementation of Perlin noise from C to Python
# ('original_perlin.c', code taken from
# https://mrl.nyu.edu/~perlin/doc/oscar.html, last accessed 2020-04-19;
# explanation by Ken Perlin on https://web.archive.org/web/
# 20160309022253/http://noisemachine.com/talk1/6.html, last accessed
# 2020-04-19). The algorithm is introduced in 'Ken Perlin. An image
# synthesizer. SIGGRAPH Comput. Graph., 19(3):287-296, July 1985'.
#
# Some comments were added to explain the implementation.

B = 0x100  # number of gradients
BM = 0xff  # used for masking (fast modulo)

N = 0x1000  # used to shift so no duplication of used gradients around
            # zero occurs (values within [-4096, 0] get shifted)
NP = 12  # unused
NM = 0xfff  # unused

p = np.zeros(B + B + 2, dtype=int)  # permutation for integer lattice
                                    # assignment

g3 = np.zeros((B + B + 2, 3), dtype=float)  # random 3D unit gradients
g2 = np.zeros((B + B + 2, 2), dtype=float)  # random 2D unit gradients
g1 = np.zeros(B + B + 2, dtype=float)  # random 1D unit gradients
start = True  # used to initialize gradients on first run


# Cubic Hermite Spline on unit interval
# p0 = m0 = m1 = 0, p1 = 1 -> 3t^2 - 2t^3
# used for smoothing
def s_curve(t):
    return t * t * (3. - 2. * t)


# linear interpolation between a and b by t
def lerp(t, a, b):
    return a + t * (b - a)


# calculate boundaries of integer lattice cell vec falls into in
# dimension i (b0, b1) and the relative position within the cell
# (r0, r1)
def setup(vec, i):
    global N, BM
    t = vec[i] + N  # used to shift so no duplication of used gradients
                    # around zero occurs (values within [-4096, 0] get
                    # shifted) so that the range [-1.0, 0.0) gets
                    # assigned -1 mod BM on the integer lattice and
                    # [0.0, 1.0] -> 0 and so that r0 is always positive
    b0 = (int(t)) & BM  # assigning integer grid value and "hashing"
    b1 = (b0 + 1) & BM  # for gradient position
    r0 = t - int(t)  # relative position within integer lattice cell
                     # in direction (b0 -> b1)
    r1 = r0 - 1.  # relative position within integer lattice cell (1 -> 0)
                  # in direction (b1 -> b0)

    return b0, b1, r0, r1


# calculate noise value at given 1D position (arg)
def noise1(arg):
    global start, p

    vec = np.zeros(1, dtype=float)
    vec[0] = arg

    # initialize gradients on first run
    if start:
        start = False
        init()

    bx0, bx1, rx0, rx1 = setup(vec, 0)  # retrieve lattice position
    sx = s_curve(rx0)  # smooth using relative position within cell

    # retrieve gradients for integer lattice points and return
    # interpolation
    u = rx0 * g1[p[bx0]]
    v = rx1 * g1[p[bx1]]

    return lerp(sx, u, v)


# calculate noise value at given 2D position (vec)
def noise2(vec):
    global start, p

    # initialize gradients on first run
    if start:
        start = False
        init()

    # retrieve lattice position for both dimensions
    bx0, bx1, rx0, rx1 = setup(vec, 0)
    by0, by1, ry0, ry1 = setup(vec, 1)

    # retrieve gradients for integer lattice points and return
    # interpolation
    i = p[bx0]
    j = p[bx1]

    b00 = p[i + by0]
    b10 = p[j + by0]
    b01 = p[i + by1]
    b11 = p[j + by1]

    sx = s_curve(rx0)
    sy = s_curve(ry0)

    def at2(rx, ry, q):
        return rx * q[0] + ry * q[1]

    q = g2[b00]
    u = at2(rx0, ry0, q)
    q = g2[b10]
    v = at2(rx1, ry0, q)
    a = lerp(sx, u, v)

    q = g2[b01]
    u = at2(rx0, ry1, q)
    q = g2[b11]
    v = at2(rx1, ry1, q)
    b = lerp(sx, u, v)

    return lerp(sy, a, b)


# calculate noise value at given 3D position (vec)
def noise3(vec):
    global start, p

    # initialize gradients on first run
    if start:
        start = False
        init()

    # retrieve lattice position for all dimensions
    bx0, bx1, rx0, rx1 = setup(vec, 0)
    by0, by1, ry0, ry1 = setup(vec, 1)
    bz0, bz1, rz0, rz1 = setup(vec, 2)

    # retrieve gradients for integer lattice points and return
    # interpolation
    i = p[bx0]
    j = p[bx1]

    b00 = p[i + by0]
    b10 = p[j + by0]
    b01 = p[i + by1]
    b11 = p[j + by1]

    t = s_curve(rx0)
    sy = s_curve(ry0)
    sz = s_curve(rz0)

    def at3(rx, ry, rz, q):
        return rx * q[0] + ry * q[1] + rz * q[2]

    q = g3[b00 + bz0]
    u = at3(rx0, ry0, rz0, q)
    q = g3[b10 + bz0]
    v = at3(rx1, ry0, rz0, q)
    a = lerp(t, u, v)

    q = g3[b01 + bz0]
    u = at3(rx0, ry1, rz0, q)
    q = g3[b11 + bz0]
    v = at3(rx1, ry1, rz0, q)
    b = lerp(t, u, v)

    c = lerp(sy, a, b)

    q = g3[b00 + bz1]
    u = at3(rx0, ry0, rz1, q)
    q = g3[b10 + bz1]
    v = at3(rx1, ry0, rz1, q)
    a = lerp(t, u, v)

    q = g3[b01 + bz1]
    u = at3(rx0, ry1, rz1, q)
    q = g3[b11 + bz1]
    v = at3(rx1, ry1, rz1, q)
    b = lerp(t, u, v)

    d = lerp(sy, a, b)

    return lerp(sz, c, d)


# normalize 2D vector
def normalize2(v):
    s = math.sqrt(v[0] * v[0] + v[1] * v[1])
    v[0] /= s
    v[1] /= s

    return v


# normalize 3D vector
def normalize3(v):
    s = math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])
    v[0] /= s
    v[1] /= s
    v[2] /= s

    return v


# initialize gradients for 1D, 2D and 3D noise
def init():
    global B, p, g1, g2, g3

    for i in range(B):
        p[i] = i

        # set random gradient value in range [0, 1-1/256] in 1/256 steps
        g1[i] = float((random() % (B + B)) - B) / B

        # set random gradient values for all dimensions
        for j in range(2):
            g2[i, j] = float((random() % (B + B)) - B) / B

        normalize2(g2[i])

        # set random gradient values for all dimensions
        for j in range(3):
            g3[i, j] = float((random() % (B + B)) - B) / B

        normalize3(g3[i])

    # create permutation of 256 elements
    for i in reversed(range(1, B)):
        j = random() % B
        k = p[i]
        p[i] = p[j]
        p[j] = k

    # duplicate gradient values (used for assignment by integer lattice)
    for i in range(B + 2):
        p[B + i] = p[i]
        g1[B + i] = g1[i]

        for j in range(2):
            g2[B + i, j] = g2[i, j]

        for j in range(3):
            g3[B + i, j] = g3[i, j]


# added to mimic 'random()' in C
def random():
    return rand.getrandbits(32)
