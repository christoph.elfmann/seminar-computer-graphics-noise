import random

from demo import demo_scenes


if __name__ == '__main__':
    """Renders the demo scenes defined in "demo/demo_scenes".
    
    The images are saved in "output/".
    """
    random.seed(42)
    resolution = (240, 135)

    demo_scenes.spheres(resolution)
    demo_scenes.perlin_red(resolution)
    demo_scenes.perlin_colorful(resolution)
    demo_scenes.perlin_globe(resolution)