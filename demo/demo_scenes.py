import time
from typing import Tuple

from basics.color import Color
from basics.vector import Vector
from lights.directional_light import DirectionalLight
from primitives.plane import Plane
from primitives.sphere import Sphere
from render.camera import Camera
from render.scene import Scene
from texturing import space_functions
from texturing.textures import PlainTexture, SolidTexture


def spheres(resolution: Tuple[int, int] = (240, 135)):
    """Render two spheres at intersection of planes.

    :param resolution: render resolution
    """
    plane1 = Plane(Vector(0, -1, 0), Vector(0, 1, 0),
                   PlainTexture(Color(20, 200, 10)))
    plane2 = Plane(Vector(-1, 0, 0), Vector(1, 0, 0),
                   PlainTexture(Color(200, 20, 10)))
    sphere1 = Sphere(Vector(0.0, 0, 3), 1,
                     PlainTexture(Color(150, 150, 0)))
    sphere2 = Sphere(Vector(1.2, 1.0, 2), 0.3,
                     PlainTexture(Color(150, 0, 150)))

    sky_light1 = DirectionalLight(Vector(-1, -1, 0.5), Color(1.0, 1.0, 1.0))

    scene = Scene(Camera(Vector(0, 0, 0), 100),
                  background_color=Color(30, 5, 128),
                  ambient_light=Color(0.2, 0.2, 0.2))
    scene.objects.append(plane1)
    scene.objects.append(plane2)
    scene.objects.append(sphere1)
    scene.objects.append(sphere2)
    scene.lights.append(sky_light1)

    path = "output/demo_spheres.png"
    print(f"Rendering '{path}'...")
    start = time.time()
    img = scene.render(resolution)
    end = time.time()
    print(f"{end - start:.2f} s")
    img.save(path)


def perlin_red(resolution: Tuple[int, int] = (240, 135)):
    """Render sphere colored using Perlin noise with red color."""
    perlin_texture = SolidTexture(Color(200, 40, 40),
                                  space_functions.noise_intensity,
                                  1)

    plane = Plane(Vector(0, -10, 0), Vector(0, 1, 0),
                  PlainTexture(Color(200, 200, 200)))
    sphere = Sphere(Vector(0.0, 0, 5), 10, perlin_texture)

    sky_light1 = DirectionalLight(Vector(-1, -1, 1), Color(1.0, 1.0, 1.0))

    scene = Scene(Camera(Vector(0, 0, -15), 100),
                  ambient_light=Color(0.2, 0.2, 0.2))
    scene.objects.append(plane)
    scene.objects.append(sphere)
    scene.lights.append(sky_light1)

    path = "output/demo_perlin_red.png"
    print(f"Rendering '{path}'...")
    start = time.time()
    img = scene.render(resolution)
    end = time.time()
    print(f"{end - start:.2f} s")
    img.save(path)


def perlin_colorful(resolution: Tuple[int, int] = (240, 135)):
    """Render sphere colored using Perlin noise with fixed palette."""
    perlin_texture = SolidTexture(Color(200, 200, 200),
                                  space_functions.noise_colorful,
                                  2)

    plane = Plane(Vector(0, -10, 0), Vector(0, 1, 0),
                  PlainTexture(Color(200, 200, 200)))
    sphere = Sphere(Vector(0.0, 0, 5), 10, perlin_texture)

    sky_light1 = DirectionalLight(Vector(-1, -1, 1), Color(1.0, 1.0, 1.0))

    scene = Scene(Camera(Vector(0, 0, -15), 100),
                  ambient_light=Color(0.2, 0.2, 0.2))
    scene.objects.append(plane)
    scene.objects.append(sphere)
    scene.lights.append(sky_light1)

    path = "output/demo_perlin_colorful.png"
    print(f"Rendering '{path}'...")
    start = time.time()
    img = scene.render(resolution)
    end = time.time()
    print(f"{end - start:.2f} s")
    img.save(path)


def perlin_globe(resolution: Tuple[int, int] = (240, 135)):
    """Render sphere colored using Perlin noise with globe-like palette."""
    perlin_texture = SolidTexture(Color(155, 155, 155),
                                  space_functions.noise_globe,
                                  5)

    plane = Plane(Vector(0, -10, 0), Vector(0, 1, 0),
                  PlainTexture(Color(200, 200, 200)))
    sphere = Sphere(Vector(0.0, 0, 5), 10, perlin_texture)

    sky_light1 = DirectionalLight(Vector(-1, -1, 1), Color(1.0, 1.0, 1.0))

    scene = Scene(Camera(Vector(0, 0, -15), 100),
                  ambient_light=Color(0.2, 0.2, 0.2))
    scene.objects.append(plane)
    scene.objects.append(sphere)
    scene.lights.append(sky_light1)

    path = "output/demo_perlin_globe.png"
    print(f"Rendering '{path}'...")
    start = time.time()
    img = scene.render(resolution)
    end = time.time()
    print(f"{end - start:.2f} s")
    img.save(path)
