from abc import ABC, abstractmethod
from typing import List

from primitives.primitive import Primitive
from basics.ray import Ray
from basics.color import Color
from basics.vector import Vector


class Light(ABC):
    """Represents source of light."""
    @abstractmethod
    def calc_color_at(self, surface_point: Vector,
                      surface_normal: Vector,
                      camera_ray: Ray,
                      scene_objects: List[Primitive]) -> Color:
        """Calculate color produced on surface by this light.
        
        :param surface_point: point on surface
        :param surface_normal: normal of surface
        :param camera_ray: ray originating from camera
        :param scene_objects: objects in scene that could block light
                              from reaching surface
        :return: 
        """
        raise NotImplementedError
