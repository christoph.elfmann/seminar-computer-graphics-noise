from typing import List

import numpy as np

from basics.color import Color
from basics.ray import Ray
from basics.vector import Vector
from lights.light import Light
from primitives.primitive import Primitive


class DirectionalLight(Light):
    """Omnipresent light with a direction and color."""

    def __init__(self, direction: Vector, color: Color):
        self.direction = direction.normalize()
        self.color = color

    def calc_color_at(self, surface_point: Vector,
                      surface_normal: Vector,
                      camera_ray: Ray,
                      scene_objects: List[Primitive]) -> Color:
        """Overrides method in 'Primitive'.

        If surface is orthogonal to light direction, color of light
        is returned; otherwise, intensity is weakened depending on
        angle.
        """
        # calculcate intensity dependent on angle between surface
        # and light direction
        intensity = -np.dot(surface_normal.coords, self.direction.coords)

        if intensity <= 0:
            return Color()

        # check if other primitives cast shadow
        # follow ray in opposite direction, starting from surface
        light_ray = Ray(surface_point, Vector(*(-self.direction.coords)))

        # return black if surface point is not lit by this light
        if light_ray.cast(scene_objects):
            return Color()

        return Color(*(intensity * self.color.values))