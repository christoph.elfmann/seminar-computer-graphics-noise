import math
from typing import Callable, Optional

import numpy as np

from basics.ray import Ray
from primitives.primitive import Primitive
from basics.color import Color
from basics.vector import Vector
from texturing.textures import Texture


class Plane(Primitive):
    """Plane defined by a position, normal and texture."""

    def __init__(self, position: Vector, normal: Vector,
                 texture: Texture):
        super().__init__()
        self.normal = normal
        self.d = np.dot(normal.coords, position.coords)
        self.texture = texture

    def calc_ray_hit_distance(self, ray: Ray) -> Optional[float]:
        """Overrides method in 'Primitive'.

        Returns 'None' if not facing ray origin. However, because of the
        way shadow is calculated for now, will cast a shadow regardless
        of normal.
        """
        dot_normals = np.dot(self.normal.coords, ray.direction.coords)

        if dot_normals >= 0:
            return None

        distance = (self.d - (np.dot(self.normal.coords, ray.origin.coords))) \
                   / dot_normals

        return distance if distance >= 0 and math.isfinite(distance) else None

    def calc_color(self, surface_point: Vector) -> Color:
        """Return color from textureOverrides method in 'Primitive'.

        Overrides method in 'Primitive'.
        """
        return self.texture.calc_color(surface_point)

    def calc_surface_normal_at(self, surface_point: Vector) -> Vector:
        """Overrides method in 'Primitive'."""
        return self.normal