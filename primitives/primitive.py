from abc import ABC, abstractmethod
from typing import Optional

from basics.color import Color
from basics.vector import Vector


class Primitive(ABC):
    """Base class for 3D objects with a simple geometry.

    Their surfaces are usually defined by an equation.
    """

    @abstractmethod
    def calc_ray_hit_distance(self, ray: 'Ray') -> Optional[float]:
        """Calculate intersection with ray and return distance.

        If an intersection between the surface and the ray is found and
        the surface is facing the ray origin, the distance from the
        ray origin in units of the ray's direction vector is returned.
        Otherwise, 'None' is returned.

        :param ray: ray to check intersection with
        :return: distance from ray origin
        """
        raise NotImplementedError

    @abstractmethod
    def calc_color(self, surface_point: Vector) -> Color:
        """Return color ar surface point."""
        raise NotImplementedError

    @abstractmethod
    def calc_surface_normal_at(self, surface_point: Vector) -> Vector:
        """Return surface normal at surface point."""
        raise NotImplementedError
