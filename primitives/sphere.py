from typing import Optional

import numpy as np

from basics.misc import solve_quadratic
from basics.ray import Ray
from primitives.primitive import Primitive
from basics.color import Color
from texturing.textures import Texture
from basics.vector import Vector


class Sphere(Primitive):
    """Sphere defined by a position, radius and texture."""

    def __init__(self, position: Vector, radius: float,
                 texture: Texture):
        """Create sphere with specified position, radius and texture."""
        super().__init__()
        self.center = position
        self.radius = radius
        self.radius_squared = self.radius ** 2
        self.texture = texture

    # https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection
    def calc_ray_hit_distance(self, ray: Ray) -> Optional[float]:
        """Overrides method in 'Primitive'.

        The assumption is made that the ray's origin is outside of the
        sphere.

        Formula takem from https://www.scratchapixel.com/lessons/
        3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/
        ray-sphere-intersection (last accessed: 2020-05-18).
        """
        tmp = ray.origin.coords - self.center.coords
        a = 1  # np.dot(ray.direction.coords, ray.direction.coords)
        b = 2 * np.dot(ray.direction.coords, tmp)
        c = np.dot(tmp, tmp) - self.radius_squared

        # calculates up to intersection points in units from ray origin
        solution = solve_quadratic(a, b, c)

        if not solution:
            return None

        closer_distance = min(solution)

        return closer_distance if closer_distance >= 0 else None

    def calc_color(self, surface_point: Vector) -> Color:
        """Return color from textureOverrides method in 'Primitive'.

        Overrides method in 'Primitive'.
        """
        return self.texture.calc_color(surface_point)

    def calc_surface_normal_at(self, surface_point: Vector) -> Vector:
        """Overrides method in 'Primitive'."""
        return Vector(*(surface_point.coords - self.center.coords)).normalize()
